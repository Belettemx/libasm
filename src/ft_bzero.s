section .text
	global _ft_bzero

_ft_bzero:
	push rdi
	cmp rdi, 0
	je end
	cmp rsi, 0
	je end
	mov [rdi], byte 0
	dec rsi

ring:
	cmp rsi, 0
	je end
	inc rdi
	cmp rdi, 0
	je end
	mov [rdi], byte 0
	dec rsi
	jmp ring

end :
	pop rdi
	ret
