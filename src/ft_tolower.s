section .text
	global _ft_tolower

_ft_tolower:
	cmp rdi, 0x40
	jle _end
	cmp rdi, 0x5a
	ja _end
	add rdi, 0x20

_end:
	mov rax, rdi
	ret
