section .text
	global _ft_isalnum

_ft_isalnum:
	cmp rdi, 0x2f
	jle _false
	cmp rdi, 0x39
	jle _true
	cmp rdi, 0x40
	jle _false
	cmp rdi, 0x5a
	jle _true
	cmp rdi, 0x60
	jle _false
	cmp rdi, 0x7a
	jle _true

_false:
	mov rax, 0
	jmp _end

_true:
	mov rax, 1

_end:
	ret
