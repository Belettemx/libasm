section .text
	global _ft_toupper

_ft_toupper:
	cmp rdi, 0x60
	jle _end
	cmp rdi, 0x7b
	jae _end
	sub rdi, 0x20

_end:
	mov rax, rdi
	ret
