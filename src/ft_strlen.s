section .text
	global _ft_strlen

_ft_strlen:
	push rdi
	xor al, al
	xor rcx, rcx
	not rcx
	cld
	repne scasb
	pop rdi
	inc rcx
	not rcx
	mov rax, rcx
	ret
