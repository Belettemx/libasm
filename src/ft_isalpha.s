section .text
	global _ft_isalpha

_ft_isalpha :
	cmp rdi, 0x40
	jle false
	cmp rdi, 0x5a
	jle true
	cmp rdi, 0x60
	jle false
	cmp rdi, 0x7a
	jle true

false :
	mov rax, 0
	jmp end

true :
	mov rax, 1

end :
	ret
