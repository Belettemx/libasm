%define MACH_SYSCALL(nb) 0x2000000 | nb
%define WRITE 4

section .text
	global _ft_putchar_fd

_ft_putchar_fd:
	enter 16, 0
	mov [rsp], rdi
	mov rdx, 1
	mov rdi, rsi
	lea rsi, [rel rsp]
	mov rax, MACH_SYSCALL(WRITE)
	syscall
	leave
	ret
