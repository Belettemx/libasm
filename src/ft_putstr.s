%define MACH_SYSCALL(nb) 0x2000000 | nb
%define STDOUT 1
%define WRITE  4

section .text
	global _ft_putstr
	extern _ft_strlen

_ft_putstr:
	cmp rdi, 0
	je end
	mov r12, rdi
	call _ft_strlen
	mov rdx, rax
	mov rdi, STDOUT
	mov rsi, r12
	mov rax, MACH_SYSCALL(WRITE)
	syscall

end:
	ret
