section .text
	global _ft_isascii

_ft_isascii:
	cmp rdi, 0x00
	jl _false
	cmp rdi, 0x7f
	ja _false

_true:
	mov rax, 1
	jmp _end

_false:
	mov rax, 0

_end:
	ret
