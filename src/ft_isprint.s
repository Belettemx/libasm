section .text
	global _ft_isprint

_ft_isprint:
	cmp rdi, 0x1f
	jle _false
	cmp rdi, 0x7f
	jl _true

_false:
	mov rax, 0
	jmp _end

_true:
	mov rax, 1

_end:
	ret
