%define MACH_SYSCALL(nb) 0x2000000 | nb
%define STDOUT 1
%define WRITE 4

section .text
	global _ft_putchar

_ft_putchar:
	enter 16, 0
	mov [rsp], rdi
	mov rdx, 1
	mov rdi, STDOUT
	lea rsi, [rel rsp]
	mov rax, MACH_SYSCALL(WRITE)
	syscall
	leave
	ret
