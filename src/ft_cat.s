%define MACH_SYSCALL(nb) 0x2000000 | nb
%define READ 3

section .text
	extern _ft_puts
	extern _ft_bzero
	global _ft_cat

_ft_cat:
	enter 4096, 0
	mov r12, rdi
	mov rdx, 4095

read:
	mov rdi, r12
	mov rdx, 4095
	mov rsi, rsp
	mov rax, MACH_SYSCALL(READ)
	syscall
	jc error
	cmp rax, 0
	je error
	mov [rsi + rax], byte 0
	mov rdi, rsi
	call _ft_puts
	jmp read

error:
	leave
	ret
