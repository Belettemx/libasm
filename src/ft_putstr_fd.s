%define MACH_SYSCALL(nb) 0x2000000 | nb
%define WRITE  4

section .text
	global _ft_putstr_fd
	extern _ft_strlen

_ft_putstr_fd:
	cmp rdi, 0
	je end
	mov r12, rdi
	mov r13, rsi
	call _ft_strlen
	mov rdx, rax
	mov rdi, r13
	mov rsi, r12
	mov rax, MACH_SYSCALL(WRITE)
	syscall

end:
	ret
