section .text
	global _ft_memcpy

_ft_memcpy:
	mov rax, rdi
	cmp rdx, 0
	je end
	xor rcx, rcx
	mov rcx, rdx
	cld
	rep movsb

end:
	ret
