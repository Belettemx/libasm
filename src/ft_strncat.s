section .text
	extern _ft_strlen
	global _ft_strncat

_ft_strncat:
	mov r8, rdi
	push r8
	cmp rsi, 0
	je null
	cmp rdi, 0
	je bis
	cmp rdx, 0
	je nullbis
	mov r13, rdx
	mov rdi, rsi
	call _ft_strlen
	mov rdi, r8
	jmp ring


null:
	cmp rdi, 0
	je nullbis
	mov rax, rdi
	ret

nullbis:
	pop rax
	ret

bis:
	mov rdx, rdi
	mov rdi, rsi
	call _ft_strlen
	mov rdi, rdx
	mov rax, rdi
	cld
	rep movsb
	pop rax
	ret

ring:
	cmp [rdi], byte 0
	je cpy
	inc rdi
	jmp ring

cpy:
	xor rcx, rcx
	cld
	inc r13
	mov rcx, r13
	rep movsb
	pop rax
	ret

