section .text
	global _ft_memset

_ft_memset:
	cmp rdi, 0
	je end
	cmp rdx, 0
	je end
	push rdi
	mov rbx, rdi
	xor rcx, rcx
	mov rax, rsi
	mov rcx, rdx
	rep stosb
	pop rdi

end:
	mov rax, rdi
	ret
