section .text
	global _ft_isdigit

_ft_isdigit:
	cmp rdi, 0x2f
	jle _false
	cmp rdi, 0x3a
	jae _false
	cmp rdi, 0x3a
	jl _true

_false:
	mov rax, 0
	jmp _end

_true:
	mov rax, 1

_end:
	ret
