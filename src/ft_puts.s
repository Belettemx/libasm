%define MACH_SYSCALL(nb) 0x2000000 | nb
%define STDOUT 1
%define WRITE  4

section .data
	null:
		.string db "(null)", 10
		.len equ $ - null.string

section .text
	global _ft_puts
	extern _ft_strlen

_ft_puts:
	cmp rdi, 0
	je empty
	mov rsi, rdi
	call _ft_strlen
	mov rdx, rax
	mov rdi, STDOUT
	mov rax, MACH_SYSCALL(WRITE)
	syscall
	jmp carriage

empty:
	mov rdx, null.len
	lea rsi, [rel null]
	mov rdi, STDOUT
	mov rax, MACH_SYSCALL(WRITE)
	syscall
	jmp end

carriage:
	lea rsi, [rel null]
	inc rsi
	inc rsi
	inc rsi
	inc rsi
	inc rsi
	inc rsi
	lea rsi, [rel rsi]
	mov rdx, 1
	mov rdi, STDOUT
	mov rax, MACH_SYSCALL(WRITE)
	syscall

end:
	ret

