section .text
	extern _malloc
	extern _ft_strlen
	extern _ft_memcpy
	extern _puts
	global _ft_strdup

_ft_strdup:
	; if (rdi == null) return (NULL)
	enter 0, 0
	cmp rdi, 0
	je null

	; r11 = rdi;
	; rdi = r12 = ft_strlen(rdi) + 1
	mov r13, rdi
	call _ft_strlen
	mov rdi, rax
	mov r12, rdi

	; if (rax = malloc(rdi) == NULL) return (NULL);
	call  _malloc
	cmp rax, 0
	je null

	; ft_memcpy(rdi, rsi, rdx);
	mov rdx, r12
	mov rdi, rax
	mov rsi, r13
	call _ft_memcpy
	mov [rax + r12], byte 0
	leave
	ret

null:
	mov rax, 0
	leave
	ret

;nullbis:
;	mov [rax], byte 0
;	ret
