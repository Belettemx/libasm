# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: agauci-d <agauci-d@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/23 12:06:26 by agauci-d          #+#    #+#              #
#    Updated: 2015/04/07 18:31:47 by agauci-d         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME  =       libasm.a
NASM  =       nasm -f macho64
LD    =       ld

CFLAGS  =     -Wall -Werror -Wextra -I ./include/
LDFLAGS =     -macosx_version_min 10.10 -lSystem

SOURCE = ./src/ft_isalpha.s ./src/ft_isdigit.s ./src/ft_isalnum.s \
		 ./src/ft_isprint.s ./src/ft_isascii.s ./src/ft_toupper.s \
		 ./src/ft_tolower.s ./src/ft_strlen.s ./src/ft_bzero.s \
		 ./src/ft_strcat.s ./src/ft_puts.s ./src/ft_memset.s ./src/ft_memcpy.s \
		 ./src/ft_putstr.s ./src/ft_strdup.s ./src/ft_cat.s \
		 ./src/ft_putstr_fd.s ./src/ft_strncat.s ./src/ft_putchar.s \
		 ./src/ft_putchar_fd.s

NOM = $(basename $(SOURCE))

OBJET = $(addsuffix .o, $(NOM))

all: $(NAME) comp

$(NAME): $(OBJET)
	@ar -rsc $(NAME) $^

comp:
	$(CC) $(CFLAGS) test.c $(NAME)
	@echo "make libasm OK"

%.o: %.s
	$(NASM) $<

clean:
	@rm -rf $(OBJET)
	@echo "clean libasm OK"

fclean:
	@rm -rf $(OBJET) $(NAME)
	@echo "fclean libasm OK"

re: fclean all
	@echo "re libasm OK"

small_coffee :
	@echo "   )  "
	@echo "  (   "
	@echo " [_]) "

coffee :
	@echo "    (     )    ( )     ( "
	@echo "     )   (    )   (    ) "
	@echo "      (  )   )     )  ( "
	@echo "        _____________ "
	@echo "       <_____________> ___"
	@echo "       |             |/ _ \ "
	@echo "       |               | | | "
	@echo "       |               |_| | "
	@echo "    ___|             |\___/  "
	@echo "   /    \___________/    \ "
	@echo "   \_____________________/ "

.PHONY: all clean fclean re coffee, small_coffee
